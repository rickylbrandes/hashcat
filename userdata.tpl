#!/bin/bash

#install tools
sudo apt-get update && sudo apt-get install -y build-essential linux-image-extra-virtual opencl-headers ocl-icd-libopencl1
echo 'blacklist nouveau' >>  /etc/modprobe.d/blacklist-nouveau.conf
echo 'blacklist lbm-nouveau' >>  /etc/modprobe.d/blacklist-nouveau.conf
echo 'options nouveau modeset=0' >>  /etc/modprobe.d/blacklist-nouveau.conf
echo 'alias nouveau off' >>  /etc/modprobe.d/blacklist-nouveau.conf
echo 'alias lbm-nouveau off' >>  /etc/modprobe.d/blacklist-nouveau.conf

#create script0
echo 'echo options nouveau modeset=0 | sudo tee -a /etc/modprobe.d/nouveau-kms.conf' >> /root/script0.sh
echo 'sudo update-initramfs -u' >> /root/script0.sh
echo 'sudo reboot' >> /root/script0.sh
chmod +x /root/script0.sh

#create script1
echo 'wget http://us.download.nvidia.com/tesla/396.44/nvidia-diag-driver-local-repo-ubuntu1604-396.44_1.0-1_amd64.deb' >> /root/script1.sh
echo 'dpkg -i nvidia-diag-driver-local-repo-ubuntu1604-396.44_1.0-1_amd64.deb' >> /root/script1.sh
echo 'apt install -y nvidia-smi' >> /root/script1.sh
echo 'sudo nvidia-smi -pm 1' >> /root/script1.sh
echo 'sudo nvidia-smi -acp 0' >> /root/script1.sh
echo 'sudo nvidia-smi — auto-boost-permission=0' >> /root/script1.sh
echo 'sudo nvidia-smi -ac 2505,875' >> /root/script1.sh
echo ''git clone https://gitlab.com/rickylbrandes/hashcat >> /root/script1.sh
echo 'wget https://hashcat.net/files/hashcat-4.2.1.tar.gz' >> /root/script1.sh
echo 'tar -xzf hashcat-4.2.1.tar.gz' >> /root/script1.sh
echo 'cd hashcat-4.2.1/ && make install' >> /root/script1.sh

chmod +x /root/script1.sh
sudo reboot
 
